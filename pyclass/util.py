import filecmp
import os
import shutil
from shutil import which, rmtree
from portmod import find_file
from portmod import get_masters

TR_PATCHER_DEPEND = "sys-bin/tr-patcher"
CLEAN_DEPEND = "sys-bin/tes3cmd"


class TRPatcher:
    def tr_patcher(self, filename: str):
        """Updates Plugins that target old versions of Tamriel Rebuilt"""
        tr_patcher_location = which("tr-patcher")
        if not tr_patcher_location:
            raise FileNotFoundError(
                "Cannot find executable tr-patcher. Please ensure it is in your PATH"
            )
        self.execute(f'{tr_patcher_location} "{filename}"')

    def src_prepare(self):
        for install_dir, plugin in self.get_files("PLUGINS"):
            if hasattr(plugin, "TR_PATCH") and plugin.TR_PATCH:
                pluginpath = os.path.join(
                    self.WORKDIR, install_dir.S, install_dir.PATH, plugin.NAME
                )
                self.tr_patcher(pluginpath)


class CleanPlugin:
    def clean_plugin(self, file: str) -> bool:
        """
        Cleans dirty GMSTs and other issues in the given Plugin File

        The plugin's master files must have been installed prior to calling this
        function
        @param filename path to the plugin to be cleaned
        @return True if the plugin needed cleaning, False if there was no change
        """
        print(f"Cleaning plugin {file}")
        tes3cmd_location = which("tes3cmd")
        if not tes3cmd_location:
            raise FileNotFoundError(
                "Cannot find executable tes3cmd. Please ensure it is in your PATH"
            )

        tmp = os.path.join(self.T, "cleaning")
        os.makedirs(tmp, exist_ok=True)
        filename = os.path.basename(file)

        # Copy file to temp dir
        shutil.copy(file, tmp)

        for master in get_masters(file):
            # Note that tes3cmd requires that the name is exactly the same
            # (including case)
            try:
                shutil.copy(find_file(master), os.path.join(tmp, master))
            except FileNotFoundError as error:
                found = False
                for localfile in os.listdir("."):
                    if os.path.basename(localfile).lower() == master.lower():
                        found = True
                        shutil.copy(localfile, os.path.join(tmp, master))
                for localfile in os.listdir(os.path.dirname(file)):
                    if os.path.basename(localfile).lower() == master.lower():
                        found = True
                        shutil.copy(
                            os.path.join(os.path.dirname(file), localfile),
                            os.path.join(tmp, master),
                        )

                if not found:
                    raise error

        olddir = os.getcwd()
        os.chdir(tmp)
        self.execute(f'{tes3cmd_location} clean "{filename}"')
        os.chdir(olddir)

        original = os.path.join(tmp, filename)
        new = os.path.join(tmp, "Clean_" + filename)

        if not os.path.exists(new) or filecmp.cmp(original, new):
            rmtree(tmp)
            return False

        print("Replacing original with cleaned file...")
        shutil.copy(new, file)
        rmtree(tmp)
        return True

    def src_prepare(self):
        for install_dir, plugin in self.get_files("PLUGINS"):
            if hasattr(plugin, "CLEAN") and plugin.CLEAN:
                pluginpath = os.path.join(
                    self.WORKDIR, install_dir.S, install_dir.PATH, plugin.NAME
                )
                self.clean_plugin(pluginpath)
