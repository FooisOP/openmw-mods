# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from portmod.pybuild import DOWNLOAD_DIR
from portmod import use_reduce


class NexusMod:
    """
    Pybuild Class for mods that are only available through NexusMods
    If NEXUS_URL is defined, automatically implements mod_nofetch
    """

    NEXUS_URL: str

    def mod_nofetch(self):
        print("Please download the following files from the url at the bottom")
        print("before continuing and move them to the download directory:")
        print(f"  {DOWNLOAD_DIR}")
        print()
        for source in self.A:
            print(f"  {source}")
        print()
        for url in use_reduce(self.NEXUS_URL, self.USE):
            print(f"  {url}?tab=files")
