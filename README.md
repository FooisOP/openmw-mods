# The OpenMW Mod Repository

![pipeline](https://gitlab.com/portmod/openmw-mods/badges/master/pipeline.svg)
[![Build status](https://ci.appveyor.com/api/projects/status/d4ojy25ybwo2t98v/branch/master?svg=true&passingText=Windows%20OK&failingText=windows%20failed)](https://ci.appveyor.com/project/portmod/openmw-mods/branch/master)

The OpenMW Mod Repository stores pybuilds, a variation on [Gentoo's Ebuilds](https://wiki.gentoo.org/wiki/Ebuild).
These build files can be used by Portmod to install OpenMW mods.

See [Portmod](https://gitlab.com/portmod/portmod/) for details on how to use this repository to install mods.

An overview of the format can be found on the [Portmod Wiki](https://gitlab.com/portmod/portmod/wikis/).
